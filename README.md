Application is built upon Scala 2.12.16 version and sbt multi-module configuration.
You need to have sbt and java setup to assemble and execute jars.

## Source code & Configuration ##
Services' source code can be found under "<project_root>/src/<service_name> folder.
Basic configuration for each service is defined in application.conf,
 e.g.
    "<project_root>/src/orders/src/resources",
It should be altered within appropriate data.

## Artifatcs ##
To assemble services navigate to project root folder and type command:

```sbtshell
sbt assembly
```
All assembled fat jars will be located under "<project_root>/artifacts" folder.

## Execution ##
To start service use command with appropriate jar
```
java -jar orders-<version>.jar
```

## Database migration ##
Database migrations should be triggered before services are up and running, to run database migration use command

```sbtshell
sbt "<service_name>/flywayMigrate" - Migrates the database
```

## API ##

Phones service:

Get all phones
```
   url
    GET locahost:9000/phones
```
Check phones existence
```
   url
    PATCH locahost:9000/phones/check
```
```
   body
    {items: ["<phone_id>, <phone_id>"]}
```    

Orders service:

Create order
```
   url
    POST localhost:9001/orders
```
```   
   body
    {
       "customer":{
          "name":"Test",
          "surname":"user",
          "email":"test@n-ix.com"
       },
       "items":[
          "<phone_id>",
          "<phone_id>"
       ]
    }
```   

## Answers ##
1. How would you improve the system? 
```
   - roll up cache solution to avoid calls to database on each phones/check request(could be done on both phones or orders service side)
   - add validation for request parameters
   - add more tests(integration etc)
   - add validation for database interactions
```

2. How would you avoid your order api to be overflowed? 
```
   Each service is stateless that means that you can easily scale it on demand. For that purpose you could use
   load balancer within appropriate balancing algorithm.
```


