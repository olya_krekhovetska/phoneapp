/**
  * Created by olgakrekhovetska on May 08, 2018
  */
object Consts {
  private val sourcesRoot = "./src"
  val commonPath = s"$sourcesRoot/common"
  val phonesPath = s"$sourcesRoot/phones"
  val ordersPath = s"$sourcesRoot/orders"

  val artifactsPath = "./artifacts"

}
