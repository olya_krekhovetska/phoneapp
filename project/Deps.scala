import sbt._

/**
  * Created by olgakrekhovetska on May 08, 2018
  */
object Deps {
  private val akkaV = "2.5.12"
  private val akkaHttpV = "10.1.1"
  val quillVersion = "2.4.2"

  private val akkaGroupId = "com.typesafe.akka"
  val akka = akkaGroupId %% "akka-actor" % akkaV //exclude ("org.slf4j", "slf4j-simple")
  val akkaHttp = akkaGroupId %% "akka-http" % akkaHttpV
  val akkaHttpPlayJson = "de.heikoseeberger" %% "akka-http-play-json" % "1.20.1"
  val typesafeConfig = "com.typesafe" % "config" % "1.3.3"
  val logback = "ch.qos.logback" % "logback-classic" % "1.2.3"
  val cats = "org.typelevel" %% "cats" % "0.9.0"

  private val quillPostgresAsyncJdbc = "io.getquill" %% "quill-async-postgres" % quillVersion
  private val postgreSql = "org.postgresql" % "postgresql" % "9.4-1200-jdbc41" exclude ("org.slf4j", "slf4j-simple")
  val quillDeps = Seq(quillPostgresAsyncJdbc, postgreSql)

  object tests {
    val scalaTest = "org.scalatest" %% "scalatest" % "3.0.1" % "test"
    val mockito = "org.mockito" % "mockito-all" % "1.10.19" % "test"

    val akkaHttpTestkit = akkaGroupId %% "akka-http-testkit" % akkaHttpV % "test"
  }
}
