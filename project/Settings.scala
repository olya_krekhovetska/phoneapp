import sbt.Keys._
import sbt._
import sbtassembly.AssemblyPlugin.autoImport.assemblyJarName
import sbtassembly.AssemblyKeys._
import Deps._
import Deps.tests.{akkaHttpTestkit, mockito, scalaTest}

/**
  * Created by olgakrekhovetska on May 08, 2018
  */
object Settings {

  val commonSettings = Seq(
    scalaVersion := "2.12.6",
    version := "0.1",
    fork in run := true,
    javaOptions += "-Dquill.macro.log=false",
    libraryDependencies ++= Seq(logback, akka, akkaHttp, akkaHttpPlayJson, cats) ++ quillDeps ++ Seq(
      scalaTest,
      akkaHttpTestkit,
      mockito)
  )

  val testSettings = Seq(fork in Test := true,
                         javaOptions in Test += "-Dconfig.resource=test.conf")
  val assemblySettings = Seq(
    assemblyJarName in assembly := s"${name.value}-${version.value}.jar")
}
