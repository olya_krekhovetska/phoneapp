import Consts._
import Deps._
import Deps.tests._
import Settings._
import com.typesafe.config.ConfigFactory
import sbt.Keys.target
import sbt.file

import scala.collection.JavaConverters._
name := "phoneapp"

version := "0.1"

scalaVersion := "2.12.6"

version := "0.1"

lazy val common =
  (project in file(commonPath))
    .settings(commonSettings)

lazy val phones = (project in file(phonesPath))
  .settings(commonSettings ++ testSettings ++ assemblySettings)
  .settings(migrations(phonesPath))
  .enablePlugins(FlywayPlugin)
  .dependsOn(common)

lazy val orders = (project in file(ordersPath))
  .settings(commonSettings ++ testSettings ++ assemblySettings)
  .settings(migrations(ordersPath))
  .enablePlugins(FlywayPlugin)
  .dependsOn(common)

lazy val migrations = (moduleRoot: String) => {
  val cfg = {
    ConfigFactory
      .parseFile(new File(s"$moduleRoot/src/main/resources/application.conf"))
      .resolve()
      .getConfig("db.postgres.migrations")
  }
  Seq(
    flywayUrl := cfg.getString("url"),
    flywayUser := cfg.getString("user"),
    flywayPassword := cfg.getString("password"),
    flywayLocations += cfg.getString("locations"),
    flywaySchemas := cfg.getStringList("schemas").asScala,
    assemblyJarName in assembly := s"${name.value}-${version.value}.jar",
    target in assembly := file(artifactsPath)
  )
}
val projects = Seq(phones, orders) ++ Seq(common)
val root = (project in file("."))
  .settings(commonSettings)
  .aggregate(projects.map(_.project): _*)
  .enablePlugins(FlywayPlugin)
