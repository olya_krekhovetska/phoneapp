package com.xyz.phones.api

import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.typesafe.config.ConfigFactory
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport
import org.scalatest.{BeforeAndAfterAll, Matchers, Suite, WordSpecLike}
import org.scalatest.exceptions.TestFailedException
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by olgakrekhovetska on May 11, 2018
  */
trait ApiTest extends BeforeAndAfterAll with Matchers with PlayJsonSupport {
  this: Suite =>
  private val log = LoggerFactory.getLogger(this.getClass)

  protected lazy val config = ConfigFactory.load()
  protected lazy implicit val ec: ExecutionContext = global

  final def doFailTest(msg: String) = throw new TestFailedException(msg, 11)

  def customBeforeAll(): Unit = {
    log.debug(
      s" \n ******* ${this.getClass.getSimpleName}: No custom beforeAll. *******\n")
    ()
  }

  def customAfterAll(): Unit = {
    log.debug(
      s" \n ******* ${this.getClass.getSimpleName}: No custom afterAll. *******\n")
    ()
  }

  final override def beforeAll(): Unit = {
    log.debug(
      s" \n *******${this.getClass.getSimpleName}: Before all is calling. *******\n")
    customBeforeAll()
    super.beforeAll()
  }

  final override def afterAll(): Unit = {
    log.debug(
      s" \n *******${this.getClass.getSimpleName}: After all is calling. *******\n")
    customAfterAll()
    super.afterAll()
  }
}
