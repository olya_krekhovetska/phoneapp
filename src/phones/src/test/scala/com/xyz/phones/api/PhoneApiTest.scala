package com.xyz.phones.api

import java.net.URL
import java.util.UUID

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.xyz.common.model.{Phone, PhoneId}
import com.xyz.phones.service.PhoneService
import org.mockito.Mockito.{times, verify, verifyNoMoreInteractions, when}
import org.scalatest.WordSpecLike
import org.scalatest.mockito.MockitoSugar

import scala.concurrent.Future

/**
  * Created by olgakrekhovetska on May 11, 2018
  */
class PhoneApiTest
    extends WordSpecLike
    with ScalatestRouteTest
    with MockitoSugar
    with ApiTest {
  import PhoneApiTest.phones
  import com.xyz.common.json.PhoneJson._

  "Phone get all API" should {
    "return appropriate phone list" in {
      val serviceMock = mock[PhoneService]
      val api = new PhoneApi(serviceMock)

      val expected = phones
      when(serviceMock.getAll).thenReturn(Future.successful(phones))

      Get("/phones") ~> api.route ~> check {
        verify(serviceMock, times(1)).getAll
        verifyNoMoreInteractions(serviceMock)
        status shouldEqual StatusCodes.OK
        responseAs[Seq[Phone]] should contain allElementsOf (expected)
      }
    }
  }
}

object PhoneApiTest {

  val phones = Seq(
    Phone(
      PhoneId(UUID.randomUUID()),
      "Iphone 4",
      "Iphone 4",
      new URL(
        "https://drop.ndtv.com/TECH/product_database/images/530201374038PM_635_iPhone_4.png"),
      300),
    Phone(PhoneId(UUID.randomUUID()),
          "Iphone 5",
          "Iphone 5",
          new URL(
            "https://cdn2.gsmarena.com/vv/pics/apple/apple-iphone-5s-ofic.jpg"),
          500)
  )

}
