CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE phones(
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    description TEXT NOT NULL,
    imageUrl TEXT NOT NULL,
    price DECIMAL NOT NULL);
