package com.xyz.phones.api

import akka.http.scaladsl.model.StatusCodes.OK
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import com.xyz.common.model.CheckPhonesReq
import com.xyz.phones.service.PhoneService
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport._

/**
  * Created by olgakrekhovetska on May 08, 2018
  */
class PhoneApi(service: PhoneService) {
  import com.xyz.phones.api.json.Json._

  val route: Route = pathPrefix("phones") {
    pathEnd {
      get {
        onSuccess(service.getAll) { phones =>
          complete((OK, phones))
        }
      }
    } ~
      path("check") {
        pathEnd {
          patch {
            entity[CheckPhonesReq](as[CheckPhonesReq]) { req =>
              onSuccess(service.checkExistence(req)) { resp =>
                complete((OK, resp))
              }
            }
          }
        }
      }
  }

}

object PhoneApi {
  def apply(service: PhoneService): PhoneApi = new PhoneApi(service)
}
