package com.xyz.phones.db

import com.xyz.common.db.DbContext
import com.xyz.common.model.PhoneId
import com.xyz.phones.service.PhoneRecord

/**
  * Created by olgakrekhovetska on May 08, 2018
  */
trait DbSchema {
  val dbCtx: DbContext
  import dbCtx._
  implicit val phoneSchemaMeta = schemaMeta[PhoneRecord]("phones")

  object phones {
    val findAll = quote(query[PhoneRecord])
    val findByIds = (ids: Seq[PhoneId]) =>
      quote(query[PhoneRecord].filter(p => liftQuery(ids).contains(p.id)))
  }

}
