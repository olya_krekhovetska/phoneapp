package com.xyz.phones.api.json

import com.xyz.common.json._

/**
  * Created by olgakrekhovetska on May 10, 2018
  */
trait AllReadsWrites
    extends PhoneIdReadsWrites
    with CheckPhonesReqReadsWrites
    with CheckPhonesRespReadsWrites
    with URLReadsWrites
    with PhoneReadsWrites

object Json extends AllReadsWrites
