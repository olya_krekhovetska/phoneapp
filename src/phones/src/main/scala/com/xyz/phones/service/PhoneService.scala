package com.xyz.phones.service

import com.xyz.common.db.DbContext
import com.xyz.common.model.{CheckPhonesReq, CheckPhonesResp, Phone}
import com.xyz.phones.db.PhoneDb

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by olgakrekhovetska on May 10, 2018
  */
class PhoneService(val dbCtx: DbContext, implicit val ec: ExecutionContext)
    extends PhoneDb {
  import PhoneService.toModel

  def getAll: Future[Seq[Phone]] = findAll.map(_.map(toModel))

  def checkExistence(data: CheckPhonesReq): Future[CheckPhonesResp] = {
    findByIds(data.ids.toSeq).map {
      case phones if phones.size == data.ids.size =>
        val foundPhones = phones.map(toModel)
        CheckPhonesResp(foundPhones.toSet)
      case phones =>
        val foundIds = phones.map(_.id)
        val foundPhones = phones.map(toModel).toSet
        CheckPhonesResp(foundPhones, data.ids.diff(foundIds.toSet))
    }
  }
}

object PhoneService {
  def toModel(rec: PhoneRecord): Phone =
    Phone(rec.id, rec.name, rec.description, rec.imageUrl, rec.price)
}
