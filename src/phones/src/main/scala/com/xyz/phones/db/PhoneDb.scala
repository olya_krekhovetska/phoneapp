package com.xyz.phones.db

import com.xyz.common.db.DbContext
import com.xyz.common.model.PhoneId

import scala.concurrent.ExecutionContext

/**
  * Created by olgakrekhovetska on May 08, 2018
  */
trait PhoneDb extends DbSchema {
  val dbCtx: DbContext
  implicit val ec: ExecutionContext
  import dbCtx._

  def findAll = dbCtx.run(phones.findAll)

  def findByIds(ids: Seq[PhoneId]) = dbCtx.run(phones.findByIds(ids))
}
