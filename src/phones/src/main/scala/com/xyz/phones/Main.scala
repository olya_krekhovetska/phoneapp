package com.xyz.phones

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import com.xyz.phones.api.PhoneApi
import com.xyz.phones.service.PhoneService
import com.xyz.common.{AppConfig, AppLauncher}
import com.xyz.common.db.DbContext
import org.slf4j.LoggerFactory

/**
  * Created by olgakrekhovetska on May 08, 2018
  */
object Main extends AppLauncher {

  lazy val appConfig = AppConfig(ConfigFactory.defaultApplication())

  implicit val system = ActorSystem(appConfig.systemName)
  implicit val materializer = ActorMaterializer.apply()(system)
  implicit val ctx = system.dispatcher

  lazy val dbContext = new DbContext("db.postgres")
  val service = new PhoneService(dbContext, ctx)
  val api = PhoneApi(service)
  val log = LoggerFactory.getLogger(this.getClass)

  log.info(s"\n *** Starting application...")
  val bindFtr =
    Http().bindAndHandle(handleExceptions(errorHandler)(api.route),
                         appConfig.host,
                         appConfig.port)

  registerShutdownHook(bindFtr, system)

}
