package com.xyz.phones.service

import java.net.URL

import com.xyz.common.model.PhoneId

/**
  * Created by olgakrekhovetska on May 08, 2018
  */
case class PhoneRecord(id: PhoneId,
                       name: String,
                       description: String,
                       imageUrl: URL,
                       price: Float)
