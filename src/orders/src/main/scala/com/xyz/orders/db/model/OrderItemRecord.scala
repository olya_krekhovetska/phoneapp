package com.xyz.orders.db.model

import java.util.UUID

import com.xyz.common.model.PhoneId
import com.xyz.orders.service.Order

/**
  * Created by olgakrekhovetska on May 11, 2018
  */
case class OrderItemRecord(id: UUID, orderId: Order.Id, phoneId: PhoneId)
