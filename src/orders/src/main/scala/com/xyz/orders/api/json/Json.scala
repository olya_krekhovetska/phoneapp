package com.xyz.orders.api.json

import com.xyz.common.json.ApiErrorReadsWrites

/**
  * Created by olgakrekhovetska on May 11, 2018
  */
trait AllReadsWrites
    extends CreateOrderReqReadsWrites
    with OrderReadsWrites
    with ApiErrorReadsWrites
object Json extends AllReadsWrites
