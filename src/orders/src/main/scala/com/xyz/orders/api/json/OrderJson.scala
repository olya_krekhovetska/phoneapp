package com.xyz.orders.api.json

import java.util.UUID

import com.xyz.common.model.PhoneId
import com.xyz.orders.service.Order
import com.xyz.orders.service.Order.{Customer}
import play.api.libs.functional.syntax._
import play.api.libs.json._

/**
  * Created by olgakrekhovetska on May 11, 2018
  */
trait OrderReadsWrites {
  import CustomerJson._
  import com.xyz.common.json.PhoneIdJson._
  import OrderIdJson._

  implicit val orderWrites: Writes[Order] =
    ((__ \ 'id).write[Order.Id] and
      (__ \ 'customer).write[Customer] and
      (__ \ 'total).write[Float] and
      (__ \ 'items).write[Seq[PhoneId]])(unlift(Order.unapply))

  implicit val orderReads: Reads[Order] =
    ((__ \ 'name).read[Order.Id] and
      (__ \ 'surname).read[Customer] and
      (__ \ 'total).read[Float] and
      (__ \ 'items).read[Seq[PhoneId]])(Order.apply _)
}
object OrderJson extends OrderReadsWrites
