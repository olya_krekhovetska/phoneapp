package com.xyz.orders.api.model

import com.xyz.common.model.PhoneId
import com.xyz.orders.service.Order.Customer

/**
  * Created by olgakrekhovetska on May 11, 2018
  */
case class CreateOrderReq(customer: Customer, items: Set[PhoneId])
