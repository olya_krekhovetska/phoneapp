package com.xyz.orders.db.model

import com.xyz.orders.service.Order

/**
  * Created by olgakrekhovetska on May 11, 2018
  */
case class OrderRecord(id: Order.Id,
                       customerName: String,
                       customerSurname: String,
                       customerEmail: String,
                       total: Float)
