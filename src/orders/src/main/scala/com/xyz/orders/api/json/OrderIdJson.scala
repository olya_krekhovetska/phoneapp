package com.xyz.orders.api.json

import java.util.UUID

import com.xyz.orders.service.Order
import play.api.libs.json._

/**
  * Created by olgakrekhovetska on May 11, 2018
  */
trait OrderIdReadsWrites {
  implicit object OrderIdWriter extends Writes[Order.Id] {
    override def writes(o: Order.Id): JsValue = JsString(o.id.toString)
  }

  implicit object OrderIdReader extends Reads[Order.Id] {
    override def reads(json: JsValue): JsResult[Order.Id] = json match {
      case JsString(str) =>
        scala.util.Try(UUID.fromString(str)) match {
          case scala.util.Success(uid) => JsSuccess(Order.Id(uid))
          case scala.util.Failure(_)   => JsError("error.expected.uuid")
        }
      case _ => JsError("error.expected.uuid")
    }
  }
}
object OrderIdJson extends OrderIdReadsWrites
