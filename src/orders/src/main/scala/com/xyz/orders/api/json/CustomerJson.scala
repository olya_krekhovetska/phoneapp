package com.xyz.orders.api.json

import com.xyz.orders.service.Order.Customer
import play.api.libs.functional.syntax._
import play.api.libs.json.{Reads, Writes, __}
import play.api.libs.json.Reads._

/**
  * Created by olgakrekhovetska on May 11, 2018
  */
trait CustomerReadsWrites {
  implicit val customerWrites: Writes[Customer] =
    ((__ \ 'name).write[String] and
      (__ \ 'surname).write[String] and
      (__ \ 'email).write[String])(unlift(Customer.unapply))

  implicit val customerReads: Reads[Customer] =
    ((__ \ 'name).read[String](minLength[String](1)) and
      (__ \ 'surname).read[String](minLength[String](1)) and
      (__ \ 'email).read[String])(Customer.apply _)
}
object CustomerJson extends CustomerReadsWrites
