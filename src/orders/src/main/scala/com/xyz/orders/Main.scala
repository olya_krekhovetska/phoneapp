package com.xyz.orders

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives.handleExceptions
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import com.xyz.orders.api.OrderApi
import com.xyz.orders.service.OrderService
import com.xyz.common.{AppConfig, AppLauncher}
import com.xyz.common.db.DbContext
import org.slf4j.LoggerFactory

/**
  * Created by olgakrekhovetska on May 08, 2018
  */
object Main extends App with AppLauncher {

  lazy val log = LoggerFactory.getLogger(this.getClass)
  lazy val config = ConfigFactory.defaultApplication()
  lazy val appConfig = AppConfig(config)

  implicit val system = ActorSystem(appConfig.systemName)
  implicit val materializer = ActorMaterializer.apply()(system)
  implicit val ctx = system.dispatcher

  lazy val dbContext = new DbContext("db.postgres")
  val http = Http()
  val service = new OrderService(config, http, dbContext, ctx, materializer)
  val api = OrderApi(service)

  log.info(s"\n *** Starting application...")
  val bindFtr =
    http.bindAndHandle(handleExceptions(errorHandler)(api.route),
                       appConfig.host,
                       appConfig.port)

  registerShutdownHook(bindFtr, system)

}
