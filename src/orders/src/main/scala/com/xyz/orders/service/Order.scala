package com.xyz.orders.service

import java.util.UUID

import com.xyz.common.model.PhoneId
import com.xyz.orders.service.Order.Customer

/**
  * Created by olgakrekhovetska on May 11, 2018
  */
case class Order(id: Order.Id,
                 customer: Customer,
                 total: Float,
                 items: Seq[PhoneId])

object Order {
  case class Id(id: UUID) extends AnyVal
  case class Customer(name: String, surname: String, email: String)
}
