package com.xyz.orders.db

import com.xyz.orders.db.model.{OrderItemRecord, OrderRecord}
import com.xyz.common.db.DbContext

/**
  * Created by olgakrekhovetska on May 11, 2018
  */
trait DbSchema {
  val dbCtx: DbContext
  import dbCtx._
  implicit val orderSchemaMeta = schemaMeta[OrderRecord]("orders")
  implicit val orderItemSchemaMeta = schemaMeta[OrderItemRecord]("order_items")

  object orders {
    val insert = (rec: OrderRecord) =>
      quote(query[OrderRecord].insert(lift(rec)))
  }

  object orderItems {
    val batchInsert = (items: List[OrderItemRecord]) =>
      quote(liftQuery(items).foreach(e => query[OrderItemRecord].insert(e)))
  }
}
