package com.xyz.orders.api

import akka.http.scaladsl.model.StatusCodes.{BadRequest, OK}
import akka.http.scaladsl.server.Directives.{
  as,
  complete,
  entity,
  onSuccess,
  path,
  post
}
import akka.http.scaladsl.server.Route
import com.xyz.orders.api.model.CreateOrderReq
import com.xyz.orders.service.OrderService
import com.xyz.orders.api.json.Json._
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport._

/**
  * Created by olgakrekhovetska on May 11, 2018
  */
class OrderApi(service: OrderService) {
  import com.xyz.common.ApiError.ApiErrorOps

  val route: Route = (path("orders") & post) {
    entity[CreateOrderReq](as[CreateOrderReq]) { req =>
      onSuccess(service.create(req)) {
        case Left(err) => complete((OK, err.toApiError))
        case Right(order) => complete((OK, order))
      }
    }
  }
}

object OrderApi {
  def apply(service: OrderService): OrderApi = new OrderApi(service)
}
