package com.xyz.orders.db

import com.xyz.common.db.DbContext
import com.xyz.orders.db.model.{OrderItemRecord, OrderRecord}

import scala.concurrent.ExecutionContext

/**
  * Created by olgakrekhovetska on May 11, 2018
  */
trait OrderDb extends DbSchema {
  val dbCtx: DbContext
  implicit val ec: ExecutionContext

  import dbCtx._

  def insert(rec: OrderRecord) = dbCtx.run(orders.insert(rec))

  def insertItems(itemRecs: List[OrderItemRecord]) =
    dbCtx.run(orderItems.batchInsert(itemRecs))
}
