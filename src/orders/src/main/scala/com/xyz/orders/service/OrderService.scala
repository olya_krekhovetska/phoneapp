package com.xyz.orders.service

import java.util.UUID

import akka.http.scaladsl.HttpExt
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.StatusCodes.OK
import akka.http.scaladsl.model.{HttpMethods, HttpRequest, RequestEntity, Uri}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import cats.data.EitherT
import com.typesafe.config.Config
import com.xyz.common.ServiceError
import com.xyz.common.db.DbContext
import com.xyz.common.model.{CheckPhonesReq, CheckPhonesResp, Phone, PhoneId}
import com.xyz.orders.api.model.CreateOrderReq
import com.xyz.orders.db.OrderDb
import cats.implicits._
import com.xyz.orders.service.OrderService.{
  OrderError,
  PhoneNotFound,
  PhoneServiceError
}
import org.slf4j.LoggerFactory
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport._
import com.xyz.common.json.CheckPhonesReqJson._
import com.xyz.common.json.CheckPhonesRespJson._
import com.xyz.orders.db.model.{OrderItemRecord, OrderRecord}
import com.xyz.orders.service.Order.Customer
import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by olgakrekhovetska on May 11, 2018
  */
class OrderService(config: Config,
                   http: HttpExt,
                   val dbCtx: DbContext,
                   implicit val ec: ExecutionContext,
                   implicit val materializer: ActorMaterializer)
    extends OrderDb {
  import OrderService.{buildOrder, toOrder}
  import dbCtx._

  private val log = LoggerFactory.getLogger(this.getClass)
  private val phoneServiceUrl = config.getString("phoneServiceUrl")

  def create(req: CreateOrderReq): Future[OrderError Either Order] = {
    (for {
      phones <- EitherT[Future, OrderError, Set[Phone]](getPhones(req.items))
      order <- EitherT(saveOrder(req, phones))
    } yield order).value
  }

  def saveOrder(req: CreateOrderReq,
                phones: Set[Phone]): Future[OrderError Either Order] = {
    val (rec, items) = buildOrder(req, phones)
    dbCtx.transaction { implicit ctx =>
      for {
        _ <- dbCtx.persistently(orders.insert(rec))
        _ <- dbCtx.run(orderItems.batchInsert(items.toList))
      } yield Right(toOrder(rec, items))
    }
  }

  def getPhones(ids: Set[PhoneId]): Future[OrderError Either Set[Phone]] = {
    val reqBody = CheckPhonesReq(ids)
    (for {
      req <- Marshal(reqBody)
        .to[RequestEntity]
        .map(
          HttpRequest()
            .withMethod(HttpMethods.PATCH)
            .withUri(Uri(phoneServiceUrl))
            .withEntity(_)
        )
      httpResp <- http.singleRequest(req)
      resp <- httpResp.status match {
        case OK =>
          Unmarshal(httpResp.entity).to[CheckPhonesResp].map {
            case CheckPhonesResp(found, notFound) if notFound.isEmpty =>
              Right[OrderError, Set[Phone]](found)
            case _ => Left[OrderError, Set[Phone]](PhoneNotFound)
          }
        case other =>
          log.warn(s"Failed to call phones service, responseStatus=$other.")
          Future.successful(Left[OrderError, Set[Phone]](PhoneServiceError))
      }
    } yield resp).recover {
      case excp =>
        log.warn(s"Failed to call phones service, reason=${excp.getMessage}.")
        Left[OrderError, Set[Phone]](PhoneServiceError)
    }
  }
}

object OrderService {
  sealed trait OrderError extends ServiceError
  case object PhoneNotFound extends OrderError {
    val key = "order.item.not_found"
  }
  case object PhoneServiceError extends OrderError {
    val key = "order.external_service.unavailable"
  }

  def buildOrder(req: CreateOrderReq,
                 phones: Set[Phone]): (OrderRecord, Set[OrderItemRecord]) = {
    val orderTotal = phones.map(_.price).reduce(_ + _)
    val orderRec = OrderRecord(Order.Id(UUID.randomUUID()),
                               req.customer.name,
                               req.customer.surname,
                               req.customer.email,
                               orderTotal)
    val items =
      phones.map(p => OrderItemRecord(UUID.randomUUID(), orderRec.id, p.id))
    (orderRec, items)
  }

  def toOrder(rec: OrderRecord, items: Set[OrderItemRecord]) =
    Order(rec.id,
          Customer(rec.customerName, rec.customerSurname, rec.customerEmail),
          rec.total,
          items.map(_.phoneId).toSeq)
}
