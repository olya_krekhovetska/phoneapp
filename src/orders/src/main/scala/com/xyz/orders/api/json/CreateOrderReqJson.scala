package com.xyz.orders.api.json

import com.xyz.common.model.PhoneId
import com.xyz.orders.api.model.CreateOrderReq
import com.xyz.orders.service.Order.Customer
import play.api.libs.functional.syntax._
import play.api.libs.json.{Reads, Writes, __}

/**
  * Created by olgakrekhovetska on May 11, 2018
  */
trait CreateOrderReqReadsWrites {
  import CustomerJson._
  import com.xyz.common.json.PhoneIdJson._
  implicit val createOrderReqWrites: Writes[CreateOrderReq] =
    ((__ \ 'customer).write[Customer] and
      (__ \ 'items).write[Set[PhoneId]])(unlift(CreateOrderReq.unapply))

  implicit val craeteOrderReqReads: Reads[CreateOrderReq] =
    ((__ \ 'customer).read[Customer] and
      (__ \ 'items).read[Set[PhoneId]])(CreateOrderReq.apply _)
}
object CreateOrderReqJson extends CreateOrderReqReadsWrites
