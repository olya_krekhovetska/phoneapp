CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE orders(
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    customerName TEXT NOT NULL,
    customerSurname TEXT NOT NULL,
    customerEmail TEXT NOT NULL,
    total DECIMAL NOT NULL);

    CREATE TABLE order_items(
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    orderId UUID NOT NULL REFERENCES orders(id),
    phoneId UUID NOT NULL);
