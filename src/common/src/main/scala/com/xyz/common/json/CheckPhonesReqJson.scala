package com.xyz.common.json

import com.xyz.common.model.{CheckPhonesReq, PhoneId}
import play.api.libs.functional.syntax._
import play.api.libs.json.{Reads, Writes, __}

/**
  * Created by olgakrekhovetska on May 10, 2018
  */
trait CheckPhonesReqReadsWrites {
  import com.xyz.common.json.PhoneIdJson._

  implicit val checkPhonesReqWriter: Writes[CheckPhonesReq] =
    (__ \ 'ids).write[Set[PhoneId]].contramap(_.ids)

  implicit val checkPhonesReqReader: Reads[CheckPhonesReq] =
    (__ \ 'ids).read[Set[PhoneId]].map(CheckPhonesReq)
}

object CheckPhonesReqJson extends CheckPhonesReqReadsWrites
