package com.xyz.common

import com.typesafe.config.Config

/**
  * Created by olgakrekhovetska on May 11, 2018
  */
case class AppConfig(systemName: String, host: String, port: Int)

object AppConfig {
  def apply(config: Config): AppConfig = {
    val host = config.getString("host")
    val port = config.getInt("port")
    val systemName = config.getString("akka.system.name")
    AppConfig(systemName, host, port)
  }
}
