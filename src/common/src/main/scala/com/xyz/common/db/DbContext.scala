package com.xyz.common.db

import java.net.URL

import com.xyz.common.db.DbContext.DbException
import io.getquill.context.async.{AsyncContext, Decoders, Encoders}
import io.getquill.{Literal, PostgresAsyncContext}
import org.slf4j.LoggerFactory

import scala.concurrent.{ExecutionContext, Future}

class DbContext(ctxPrefix: String)
    extends PostgresAsyncContext(Literal, ctxPrefix)
    with DbContext.RichDecoders
    with DbContext.RichEncoders {
  private val log = LoggerFactory.getLogger(this.getClass)

  def persistently[E](f: => Quoted[Action[E]])(
      implicit ec: ExecutionContext): Future[Long] = run(f).map {
    case 1L => 1L
    case other =>
      val msg = s"Failed to process db action, reason=$other."
      log.warn(msg)
      throw DbException(msg)
  }
}

object DbContext {

  trait RichEncoders extends Encoders {
    this: AsyncContext[_, _, _] =>

    implicit val urlEncoder = MappedEncoding[URL, String](_.toString)
  }

  trait RichDecoders extends Decoders {
    this: AsyncContext[_, _, _] =>

    implicit val urlDecoder = MappedEncoding[String, URL](str => new URL(str))
  }

  case class DbException(msg: String) extends RuntimeException(msg)
}
