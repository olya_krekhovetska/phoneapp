package com.xyz.common

/**
  * Created by olgakrekhovetska on May 11, 2018
  */
trait ServiceError {
  val key: String
}
