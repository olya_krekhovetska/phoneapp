package com.xyz.common

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import org.slf4j.LoggerFactory

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by olgakrekhovetska on May 10, 2018
  */
trait AppLauncher extends App with ErrorHandler {
  private lazy val log = LoggerFactory.getLogger(this.getClass)

  protected def registerShutdownHook(bindHttpFtr: Future[Http.ServerBinding],
                                     system: ActorSystem): Unit = {
    Runtime.getRuntime.addShutdownHook {
      new Thread(
        () =>
          bindHttpFtr
            .flatMap(_.unbind())
            .onComplete { _ =>
              log.info(s"*** Gracefully shut down http service ***\n")
              Await.ready(system.terminate(), 30 seconds).onComplete { _ =>
                log.info(s"*** Gracefully shut down actor system... ***\n")
              }
          })
    }
  }
}
