package com.xyz.common.json

import java.net.URL

import play.api.libs.json._

/**
  * Created by olgakrekhovetska on May 08, 2018
  */
trait URLReadsWrites {

  implicit object URLWriter extends Writes[URL] {
    def writes(url: URL) = JsString(url.toString)
  }

  implicit object URLReader extends Reads[URL] {
    override def reads(json: JsValue): JsResult[URL] = json match {
      case JsString(str) =>
        scala.util.Try(new URL(str)) match {
          case scala.util.Success(url) => JsSuccess(url)
          case scala.util.Failure(_)   => JsError("error.expected.url")
        }
      case _ => JsError("error.expected.url")
    }
  }
}

object URLJson extends URLReadsWrites
