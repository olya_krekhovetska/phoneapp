package com.xyz.common.model

/**
  * Created by olgakrekhovetska on May 10, 2018
  */
case class CheckPhonesReq(ids: Set[PhoneId])
