package com.xyz.common.json

import java.net.URL

import com.xyz.common.model.{Phone, PhoneId}
import play.api.libs.functional.syntax._
import play.api.libs.json.{Reads, Writes, __}

/**
  * Created by olgakrekhovetska on May 08, 2018
  */
trait PhoneReadsWrites {
  import com.xyz.common.json.PhoneIdJson._
  import URLJson._

  implicit val phoneJsonWriter: Writes[Phone] = (
    (__ \ 'id).write[PhoneId] and
      (__ \ 'name).write[String] and
      (__ \ 'description).write[String] and
      (__ \ 'imageUrl).write[URL] and
      (__ \ 'price).write[Float]
  )(unlift(Phone.unapply))

  implicit val phoneJsonReader: Reads[Phone] = (
    (__ \ 'id).read[PhoneId] and
      (__ \ 'name).read[String] and
      (__ \ 'description).read[String] and
      (__ \ 'imageUrl).read[URL] and
      (__ \ 'price).read[Float]
  )(Phone.apply _)

}
object PhoneJson extends PhoneReadsWrites
