package com.xyz.common.json

import java.util.UUID

import com.xyz.common.model.PhoneId
import play.api.libs.json._

/**
  * Created by olgakrekhovetska on May 10, 2018
  */
trait PhoneIdReadsWrites {
  implicit object PhoneIdWriter extends Writes[PhoneId] {
    override def writes(o: PhoneId): JsValue = JsString(o.id.toString)
  }

  implicit object PhoneIdReader extends Reads[PhoneId] {
    override def reads(json: JsValue): JsResult[PhoneId] = json match {
      case JsString(str) =>
        scala.util.Try(UUID.fromString(str)) match {
          case scala.util.Success(uid) => JsSuccess(PhoneId(uid))
          case scala.util.Failure(_)   => JsError("error.expected.uuid")
        }
      case _ => JsError("error.expected.uuid")
    }
  }
}
object PhoneIdJson extends PhoneIdReadsWrites
