package com.xyz.common.json

import com.xyz.common.model.{CheckPhonesResp, Phone, PhoneId}
import play.api.libs.functional.syntax._
import play.api.libs.json.{Reads, Writes, __}

/**
  * Created by olgakrekhovetska on May 10, 2018
  */
trait CheckPhonesRespReadsWrites {
  import com.xyz.common.json.PhoneIdJson._
  import com.xyz.common.json.PhoneJson._

  implicit val checkPhonesRespWriter: Writes[CheckPhonesResp] =
    ((__ \ 'found).write[Set[Phone]] and
      (__ \ 'notFound).write[Set[PhoneId]])(unlift(CheckPhonesResp.unapply))

  implicit val checkPhonesRespReader: Reads[CheckPhonesResp] =
    ((__ \ 'found).read[Set[Phone]] and
      (__ \ 'notFound).read[Set[PhoneId]])(CheckPhonesResp.apply _)
}

object CheckPhonesRespJson extends CheckPhonesRespReadsWrites
