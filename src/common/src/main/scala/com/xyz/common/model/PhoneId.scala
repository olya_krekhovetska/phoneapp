package com.xyz.common.model

import java.util.UUID

/**
  * Created by olgakrekhovetska on May 08, 2018
  */
case class PhoneId(id: UUID) extends AnyVal
