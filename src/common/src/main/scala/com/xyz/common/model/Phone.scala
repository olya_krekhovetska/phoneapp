package com.xyz.common.model

import java.net.URL

/**
  * Created by olgakrekhovetska on May 11, 2018
  */
case class Phone(id: PhoneId,
                 name: String,
                 description: String,
                 imageUrl: URL,
                 price: Float)
