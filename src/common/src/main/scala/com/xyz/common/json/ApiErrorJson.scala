package com.xyz.common.json

import com.xyz.common.ApiError
import play.api.libs.json._

/**
  * Created by olgakrekhovetska on May 11, 2018
  */
trait ApiErrorReadsWrites {
  implicit object ApiErrorWriter extends Writes[ApiError] {
    override def writes(o: ApiError): JsValue =
      JsObject(Seq(ApiError.errorKey -> JsString(o.errorCode)))

    implicit val apiErrorReader: Reads[ApiError] =
      (__ \ ApiError.errorKey).read[String].map(ApiError.apply _)
  }
}
object ApiErrorJson extends ApiErrorReadsWrites
