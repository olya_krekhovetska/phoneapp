package com.xyz.common

/**
  * Created by olgakrekhovetska on May 11, 2018
  */
case class ApiError(errorCode: String)

object ApiError {
  val errorKey = "error"
  def apply(err: ServiceError): ApiError = ApiError(err.key)
  def apply(errorCode: String): ApiError = ApiError(errorCode)

  implicit class ApiErrorOps[T <: ServiceError](error: T) {
    def toApiError = ApiError(error)
  }
}
