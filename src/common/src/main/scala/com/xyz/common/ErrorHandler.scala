package com.xyz.common

import akka.http.scaladsl.server.ExceptionHandler
import org.slf4j.LoggerFactory
import akka.http.scaladsl.model.StatusCodes.InternalServerError
import akka.http.scaladsl.server.Directives.{complete, extractUri}

/**
  * Created by olgakrekhovetska on May 08, 2018
  */
trait ErrorHandler {
  private val log = LoggerFactory.getLogger(this.getClass)

  val errorHandler = ExceptionHandler {
    case excp =>
      extractUri { uri =>
        log.error(
          s"Cannot handle request to $uri due to reason=${excp.getMessage}, exceptionType=${excp.getClass}.")
        complete(
          (InternalServerError,
           s"Something ſwent wrong, reason=${excp.getMessage}."))
      }
  }
}
